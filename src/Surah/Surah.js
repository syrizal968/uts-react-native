import { FlatList, 
    SafeAreaView, 
    Text, 
    TouchableOpacity, 
    View,
    Image
  } from 'react-native'
  import React, { useState, useEffect } from 'react'
  import { useNavigation } from "@react-navigation/native"
  import axios from 'axios'
  import SearchBar from "react-native-dynamic-search-bar";
  import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
  
const Tab = createMaterialTopTabNavigator();
const BASE_URL = 'https://quran-endpoint.vercel.app/'

const   Surah = () => {

   const navigation = useNavigation(); 
    const [surah, setSurah]= useState([])

    const getData = async () =>{
      try{
        axios.get(`${BASE_URL}quran`).then( res => {
          // console.log(res.data.data)
          setSurah(res.data.data)
        })
      }
      catch (error){
        alert(error)
      }
    }


  
    useEffect(()=>{
      getData()
    }, [])
  
  return (
    <SafeAreaView style={{flex:1}}>
    <FlatList
      data = {surah}
      renderItem = {(item, index) => (
        <TouchableOpacity  style={{flexDirection: 'row',
          padding:'8%',  
          justifyContent:'space-around'      
        
        }}
        onPress={()=> navigation.navigate('DetailSurah', {surahId: `${BASE_URL}quran/${item.item.number}`})}>
        <View style={{margin:'2%', justifyContent:'center',
          alignItems:'center'}}>
          {/* <Image style={{height:'160%',
          width:'250%',
          position:"absolute"
     }}resizeMode = 'cover'
    source={require('../assets/5.png')} /> */}
          <Text style={{color:'#ffff',
          fontSize:28,
          alignItems:'center',
          margin:5,
          }}
          >
          {item.item.number}
          </Text>
         </View>

         <View style={{flex:1,
          flexDirection:'row',
          justifyContent:'space-between', 
          alignItems:'center',
          margin:5,
          left:'4%'
          }}>
          <View>


          <Text style={{color:'#ffff',
          fontSize:16
          }}>
          {item.item.asma.id.short}</Text>
          <Text style={{color:'#ffff',
          fontSize:13
          }}>
          {item.item.type.id} - {item.item.ayahCount} Ayat</Text>
          </View>
          <Text style={{color:'#ffff',
          fontSize:20
          }}>{item.item.asma.ar.short}</Text>
          </View>
        </TouchableOpacity>
      )}
       />
    </SafeAreaView>
  )
}

export default Surah