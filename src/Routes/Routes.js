import { Text, View } from 'react-native';
import React from 'react';
import Screen from '../Screen/Screen';
import Home from '../Home/Home';
import Surah from '../Surah/Surah';
import DetailSurah from '../DetailSurah/DetailSurah';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();


const Routes = () => {
  return (
    <Stack.Navigator>
        <Stack.Screen name="Screen" component={Screen} options={{headerShown: false}} />
        <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
        <Stack.Screen name="Surah" component={Surah} options={{headerShown: false}} />
        <Stack.Screen name="DetailSurah" component={DetailSurah} options={{headerShown: false}} />
    </Stack.Navigator>
  )
}

export default Routes