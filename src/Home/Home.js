import { FlatList, 
  SafeAreaView, 
  Text, 
  TouchableOpacity, 
  View,
  Image
} from 'react-native'
import React, { useState, useEffect } from 'react'
import { useNavigation } from "@react-navigation/native"
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Surah from '../Surah/Surah';

const Tab = createMaterialTopTabNavigator();
 

const Home = () => {
  const navigation = useNavigation(); 

  return (
    
    <View style={{flex: 1,
    backgroundColor: '#091945' ,}}>
    <View style={{marginTop:'15%',
    left: '15%',
    }}>
    <Text style={{color:'#ffff',
    textAlign:'left',
    fontSize: 25
    }}>Quran App</Text>
    </View>
    <View style={{flex:0.3,
      alignItems:'center',
    marginTop:'10%'
    }}>
    <Image style={{height:'65%',
    width:'85%',
    position: 'absolute',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    }}resizeMode = 'cover'
    source={require('../assets/2.png')}
     />
    </View>
    <Surah />
    <View style={{
      // backgroundColor:'#7E57'
      }}>

      </View>   
    </View> 
    
  )


}

export default Home
