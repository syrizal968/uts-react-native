import { View, Text, Image, TouchableOpacity, StatusBar } from 'react-native'
import React from 'react'
import { useNavigation } from "@react-navigation/native"
import { SafeAreaView } from 'react-native-safe-area-context';

const Screen = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={{flex: 1,
    backgroundColor: '#091945',
     }}>
     <StatusBar barStyle="light-content" backgroundColor="#651FFF" />   
    <View style={{justifyContent: 'center',
    alignItems: 'center',
    marginTop:'10%',
    marginBottom:'10%'
    }}>
      <Text style={{fontSize: 28,
    fontWeight: "700",
    fontFamily: 'Poppins',
    color: '#ffffff',
    }}>App Al-Qur'an</Text>
    <Text style={{color: '#7b80ad',
    fontFamily: 'Poppins',
    fontWeight: 'regular',
    fontSize: 18,
    marginBottom: '2%',
    marginTop: '10%'
    }}>Pelajari Al-Qur'an </Text>
    <Text style={{color: '#7b80ad', 
    fontFamily: 'Poppins',
    fontWeight: 'regular',
    fontSize: 18,

    }}>dan Bacalah sekali setiap hari</Text>
    </View>
    <View style={{flex : 0.7,
    // backgroundColor :  '#ffffff',
    justifyContent: 'center',
    alignItems:'center',
    }}>
    <Image style={{width: '90%',
    height: '100%',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    position: 'absolute',}}
    resizeMode="cover"
    source={require("../assets/b.png")}
    />
    <TouchableOpacity style={{alignItems: "center",
    backgroundColor: "#f9b091",
    justifyContent:'center',
    height:'16%',
    width:'45%',
    borderRadius: 25,
    top:'49%'
    }}
        onPress={()=> navigation.navigate("Home")}
      >
        <Text style={{textAlign: "center",
    fontSize: 20,
    color: "#091945",
    verticalAlign: 'top',
    fontFamily: 'Poppins',
    // fontWeight: "700",
    }}>Baca Al-qur'an</Text>
      </TouchableOpacity>
    </View>
    </SafeAreaView>
  )
}

export default Screen