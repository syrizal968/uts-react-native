import {  Text, View,Image, TouchableOpacity, SafeAreaView, FlatList} from 'react-native'
import React, { useEffect , useState} from 'react'
import axios from 'axios'


const DetailSurah = ({route}) => {
  
  const [listAyat, setlistAyat] = useState([])

    const getAyat = async () =>{
      try{
        axios.get(route.params.surahId).then( res => {
          // console.log(res.data.data.ayahs)
          setlistAyat(res.data.data.ayahs)
        })
      }
      catch (error){
        alert(error)
      }
    }
  
    useEffect(()=>{
      getAyat()
    }, [])

  return (
    <View style={{flex:1 ,backgroundColor: '#091945'}}>
     <View style={{marginTop:'10%',
    }}>
    <Text style={{color:'#ffff',
    textAlign:'center',
    fontSize: 25
    }}>Quran App</Text>
    </View>
    <View style={{flex:0.3,
      alignItems:'center',
    marginTop:'10%'
    }}>
    <Image style={{height:'65%',
    width:'85%',
    position: 'absolute',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    }}resizeMode = 'cover'
    source={require('../assets/2.png')}
     />
    </View>
    <SafeAreaView style={{flex:1}}>
    <FlatList
      data = {listAyat}
      renderItem = {(item, index) => (
        <View  style={{flexDirection: 'row',
          padding:'8%',  
          justifyContent:'space-around'      
        
        }}>
     

         <View style={{flex:1,
          justifyContent:'space-between', 
          // alignItems:'center',
          margin:5,
          left:'4%'
          }}>
          <View>
          <Text style={{color:'#ffff',
          fontSize:20
          }}>
          {item.item.number.insurah}
          </Text>
          <Text style={{color:'#ffff',
          fontSize:25
          }}>
          {item.item.text.ar}
          </Text>
          </View>
          <Text style={{color:'#ffff',
          marginTop:30,
          fontSize:18
          }}>
          {item.item.translation
.id}
          </Text>
          </View>
        </View>
      )}
       />
    </SafeAreaView>
    </View>
  )
}

export default DetailSurah
